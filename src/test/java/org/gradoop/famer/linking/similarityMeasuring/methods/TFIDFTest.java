package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.TFIDFVariant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class TFIDFTest {

  private final TFIDFVariant tfidfVariant;

  public TFIDFTest(TFIDFVariant tfidfVariant) {
    this.tfidfVariant = tfidfVariant;
  }

  @Parameterized.Parameters
  public static Iterable<Object[]> tfidfVariant() {
    return Arrays.asList(new Object[][] {
      {TFIDFVariant.L_T}, {TFIDFVariant.N_T}, {TFIDFVariant.A_T},
    });
  }

  @Test
  public void testComputeSameSentenceSimilarity() throws Exception {
    final String s1 = "This is a sentence";
    final String s2 = "This is a sentence";
    Map<String, Integer> documentFrequency = new HashMap<String, Integer>() {{
      put("This", 2);
      put("is", 2);
      put("a", 2);
      put("sentence", 2);
    }};

    double similarity = new TFIDF(documentFrequency, " ", 2, tfidfVariant).computeSimilarity(s1, s2);

    assertEquals(1, similarity, 0);
  }

  @Test
  public void testComputeSimilarityFromProperty() throws Exception {

    Map<String, Integer> documentFrequency = new HashMap<String, Integer>() {{
      put("this", 2);
      put("is", 2);
      put("a", 2);
      put("sentence", 2);
    }};
    final PropertyValue s1 = PropertyValue.create("This is a sentence");
    final PropertyValue s2 = PropertyValue.create("This is a sentence");

    double similarity = new TFIDF(documentFrequency, " ", 2, tfidfVariant).computeSimilarity(s1, s2);

    assertEquals(1, similarity, 0.0);
  }

  @Test
  public void testComputeSimilarity() throws Exception {

    Map<String, Integer> documentFrequency = new HashMap<String, Integer>() {{
      put("This", 2);
      put("is", 2);
      put("a", 2);
      put("sentence", 2);
      put("with", 2);
      put("some", 2);
      put("words", 2);
      put("also", 1);
      put("I", 1);
      put("am", 1);
      put("completely", 1);
      put("different", 1);
    }};
    final String s1 = "This is a sentence with some words";
    final String s2 = "This is also a sentence with some words";
    final String s3 = "I am completely different";

    double sim1_2 = new TFIDF(documentFrequency, " ", 3, tfidfVariant).computeSimilarity(s1, s2);
    double sim1_3 = new TFIDF(documentFrequency, " ", 3, tfidfVariant).computeSimilarity(s1, s3);

    assertTrue(sim1_2 > sim1_3);
    assertTrue(sim1_2 < 1);
  }

  @Test
  public void testComputeSimilarityEmptySentences() throws Exception {

    Map<String, Integer> documentFrequency = new HashMap<String, Integer>() {{
      put("This", 1);
      put("is", 1);
      put("a", 1);
      put("sentence", 1);
    }};

    final String s1 = "This is a sentence";
    final String s2 = " ";

    double similarity = new TFIDF(documentFrequency, " ", 2, tfidfVariant).computeSimilarity(s1, s2);
    assertEquals(0, similarity, 0.0);
  }
}