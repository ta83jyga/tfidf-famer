package org.gradoop.famer.linking.similarityMeasuring.methods.functions;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.gradoop.famer.linking.linking.Linker.GRAPH_LABEL;
import static org.junit.Assert.assertTrue;

public class DocumentFrequencyTest extends GradoopFlinkTestBase {

  @Test
  public void testExtractWordsFunction() throws Exception {
    List<String> sourceAttribute = Collections.singletonList("description");
    String tokenizer = " ";
    String graphString = "input[" +
      "(cellphone:Product {id:1, description:\"A good cellphone\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(camera:Product {id:2, description:\"Great camera\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(laptop:Product {id:3, description:\"Fastest laptop\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(monitor:Product {id:4, description:\"Clear   Pictures\", " + GRAPH_LABEL + ":\"g1\"})" + "]";

    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");
    final List<Tuple2<GradoopId, String>> docToWord =
      inputGraph.getVertices().flatMap(new TokenizeAttribute(sourceAttribute, tokenizer)).collect();

    final List<String> words = docToWord.stream()
      .map(tuple -> tuple.f1)
      .collect(Collectors.toList());

    assertTrue(
      words.containsAll(asList(
        "a","good", "cellphone",
        "great", "camera",
        "fastest", "laptop",
        "clear", "pictures"
      )));
  }

  @Test
  public void testCountDocumentFrequencyFunction() throws Exception {
    final GradoopId id1 = GradoopId.get();
    final GradoopId id2 = GradoopId.get();
    final GradoopId id3 = GradoopId.get();

    DataSet<Tuple2<GradoopId, String>> docToWord =
      getExecutionEnvironment().fromCollection(
        asList(
          Tuple2.of(id1, "first"),
          Tuple2.of(id1, "second"),
          Tuple2.of(id2, "third"),
          Tuple2.of(id3, "first"),
          Tuple2.of(id3, "second")
        )
      );

    final List<Tuple2<String, Integer>> wordCount = docToWord
      .groupBy(1)
      .reduceGroup(new CountDocumentFrequency())
      .collect();

    assertTrue(
      wordCount.containsAll(asList(
        Tuple2.of("first", 2),
        Tuple2.of("second", 2),
        Tuple2.of("third", 1)
      )));
  }

  @Test
  public void testExtractAndCountWords() throws Exception {
    List<String> sourceAttribute = Collections.singletonList("description");
    String tokenizer = " ";
    String graphString = "input[" +
      "(cellphone:Product {id:1, description:\"A good cellphone\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(cellphone2:Product {id:2, description:\"A very good cellphone\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(laptop:Product {id:3, description:\"Fastest laptop\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(monitor:Product {id:4, description:\"Clear   Pictures\", " + GRAPH_LABEL + ":\"g1\"})" + "]";

    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");
    final List<Tuple2<String, Integer>> result =
      inputGraph.getVertices().flatMap(new TokenizeAttribute(sourceAttribute, tokenizer))
        .groupBy(1)
        .reduceGroup(new CountDocumentFrequency())
        .collect();

    assertTrue(
      result.containsAll(asList(
        Tuple2.of("a", 2),
        Tuple2.of("very", 1),
        Tuple2.of("good", 2),
        Tuple2.of("cellphone", 2)
      )));
  }
}