package org.gradoop.famer.linking.similarityMeasuring;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponentBaseConfig;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityFieldList;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.TFIDFBasedSimilarityComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.TFIDFSimilarityComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.TFIDFVariant;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.operators.count.Count;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.gradoop.famer.linking.linking.Linker.GRAPH_LABEL;
import static org.junit.Assert.assertFalse;

public class SimilarityMeasurerWithDocumentFrequencyBroadcastTest extends GradoopFlinkTestBase {

  @Test
  public void testDocumentFrequencyMapAndDocumentsCountIsBroadcasted() throws Exception {
    String label = "product";

    final SimilarityComponentBaseConfig config =
      new SimilarityComponentBaseConfig("tfidf", "*", label, "description", "*", label, "description", 1.0);
    List<SimilarityComponent> similarityComponents =
      Collections.singletonList(new TFIDFSimilarityComponent(config, " ", TFIDFVariant.N_T));

    EPGMVertex vertexA = createVertex(label, "1", "A good cellphone");
    EPGMVertex vertexB = createVertex(label, "2", "A good cellphone");

    final DataSet<Tuple2<String, Integer>> documentFrequencies = getExecutionEnvironment()
      .fromElements(Tuple2.of("a", 2), Tuple2.of("good", 2), Tuple2.of("cellphone", 2));

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> blockedVertices =
      getExecutionEnvironment().fromElements(Tuple2.of(vertexA, vertexB));
    final DataSet<Long> documentsCount = Count.count(blockedVertices);

    final List<Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>> similarityMeasurements =
      blockedVertices.flatMap(new SimilarityMeasurerWithDocumentFrequencyBroadcast(similarityComponents))
        .withBroadcastSet(documentFrequencies,
          TFIDFBasedSimilarityComponent.DOCUMENT_FREQUENCY_BROADCAST_VARIABLE)
        .withBroadcastSet(documentsCount, TFIDFBasedSimilarityComponent.DOCUMENTS_COUNT_BROADCAST_VARIABLE)
        .collect();

    similarityMeasurements.forEach(tuple -> assertFalse(tuple.f2.isEmpty()));
  }

  private EPGMVertex createVertex(String label, String id, String description) {

    Properties properties = new Properties();
    properties.set("id", id);
    properties.set("description", description);
    properties.set(GRAPH_LABEL, "graphLabel");

    EPGMVertex vertex = new EPGMVertex();
    vertex.setId(GradoopId.get());
    vertex.setLabel(label);
    vertex.setProperties(properties);

    return vertex;
  }
}