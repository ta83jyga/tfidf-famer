package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SoftTFIDFTest {

  @Test
  public void testComputeSimilarity() throws Exception {

    Map<String, Integer> documentFrequency = new HashMap<String, Integer>() {{
      put("This", 30);
      put("is", 300);
      put("a", 430);
      put("sentence", 1);
      put("seantence", 1);
      put("text", 1);
      put("with", 3);
      put("some", 3);
      put("words", 3);
      put("also", 1);
      put("am", 1);
      put("completely", 1);
      put("different", 1);
    }};
    final String s1 = "This is a sentence with some words";
    final String s2 = "This is a seantence with some words";
    final String s3 = "This is a text with some words";
    final String s4 = "This is completely different";

    final SoftTFIDF softTFIDF = new SoftTFIDF(documentFrequency, " ", 3, 0.9);
    double sim1_2 = softTFIDF.computeSimilarity(s1, s2);
    double sim1_3 = softTFIDF.computeSimilarity(s1, s3);
    double sim1_4 = softTFIDF.computeSimilarity(s1, s4);

    assertTrue(sim1_2 + " should be greater than " + sim1_3, sim1_2 > sim1_3);
    assertTrue(sim1_2 + " should be greater than " + sim1_4, sim1_2 > sim1_4);
  }

  @Test
  public void testComputeSimilarityWithEmptySentence() throws Exception {

    Map<String, Integer> documentFrequency = new HashMap<String, Integer>() {{
      put("sentence", 2);
    }};

    final String s1 = "sentence";
    final String s2 = "";

    double sim = new SoftTFIDF(documentFrequency, " ", 2000, 0.9).computeSimilarity(s1, s2);

    assertEquals(0, sim, 0);
  }
}