package org.gradoop.famer.preprocessing.io.benchmarks;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple5;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GTFileComponent;
import org.gradoop.famer.preprocessing.io.common.GraphToGraphCollection;
import org.gradoop.flink.io.impl.graph.GraphDataSource;
import org.gradoop.flink.io.impl.graph.tuples.ImportEdge;
import org.gradoop.flink.io.impl.graph.tuples.ImportVertex;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.bool.False;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to read the Camera products benchmark data.
 */
public class CameraReader extends BenchmarkSetReaderBase<DataSet<Tuple2<String, String>>> {

  private final String[] sources;

  public CameraReader(String... sources) {
    this.sources = sources;
  }

  /**
   * @return null
   * @deprecated Use {@link GTFileComponent}
   */
  @Override
  @Deprecated
  public DataSet<Tuple2<String, String>> getPerfectMapping(String folderPath) {
    return null;
  }

  @Override
  public GraphCollection getBenchmarkDataAsGraphCollection(String folderPath) {
    List<LogicalGraph> graphs = new ArrayList<>();

    for (String source : sources) {
      LogicalGraph graph = parseEntriesToLogicalGraph(folderPath + "/" + source + ".csv", source);
      if (graph != null) {
        graphs.add(graph);
      }
    }

    return GraphToGraphCollection.execute(graphs);
  }

  /**
   * Reads the file data and builds an edgeless {@link LogicalGraph} with all entries as vertices.
   *
   * @param filePath  Path to the data file
   * @param graphName The name of the graph data
   * @return {@link LogicalGraph} with all entries as vertices
   */
  private LogicalGraph parseEntriesToLogicalGraph(String filePath, String graphName) {
    DataSet<Tuple5<String, String, String, String, String>> entries =
      getEnv().readCsvFile(filePath).parseQuotedStrings('`').fieldDelimiter(",").ignoreFirstLine()
        .types(String.class, String.class, String.class, String.class, String.class);

    DataSet<ImportVertex<String>> importVertices = entries
      .map((MapFunction<Tuple5<String, String, String, String, String>, ImportVertex<String>>) tuple -> {
        Map<String, Object> properties = new HashMap<>();
        properties.put("title", tuple.f1);
        properties.put("brand", tuple.f2);
        properties.put("all", tuple.f3);
        properties.put("title2", tuple.f4);
        properties.put(GRAPH_LABEL_PROPERTY, graphName);
        return new ImportVertex<>(tuple.f0, graphName, Properties.createFromMap(properties));
      }).returns(new TypeHint<ImportVertex<String>>() {
      });

    DataSet<ImportEdge<String>> importEdges =
      getEnv().fromElements(new ImportEdge<>("0", "0", "1")).filter(new False<>());

    LogicalGraph graph =
      new GraphDataSource<>(importVertices, importEdges, "id", getConfig()).getLogicalGraph();
    graph = graph.transformGraphHead((current, transformed) -> {
      current.setLabel(graphName);
      return current;
    });
    return graph;
  }
}
