package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.apache.flink.api.java.tuple.Tuple3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.apache.commons.lang.StringUtils.isBlank;

public class SoftTFIDF extends TFIDFBasedSimilarityComputation {

  /**
   * Threshold for first level similarity measurement
   */
  private final double threshold;

  private final SimilarityComputation<String> similarityMeasurer;

  /**
   * @param documentFrequency  a mapping of the document frequency to each word
   * @param tokenizer          sentence tokenizer
   * @param documentsCount     total amount of documents
   * @param threshold          token similarity threshold
   * @param similarityMeasurer Token similarity measurer
   */
  public SoftTFIDF(final Map<String, Integer> documentFrequency, final String tokenizer, long documentsCount,
    double threshold, SimilarityComputation<String> similarityMeasurer) {
    super(tokenizer, documentFrequency, documentsCount);
    this.threshold = threshold;
    this.similarityMeasurer = similarityMeasurer;
  }

  /**
   * @param documentFrequency a mapping of the document frequency to each word
   * @param tokenizer         sentence tokenizer
   * @param documentsCount    total amount of documents
   * @param threshold         token similarity threshold
   */
  public SoftTFIDF(final Map<String, Integer> documentFrequency, final String tokenizer, long documentsCount,
    double threshold) {
    super(tokenizer, documentFrequency, documentsCount);
    this.threshold = threshold;
    this.similarityMeasurer = new JaroWinkler();
  }

  @Override
  public double computeSimilarity(String atr1, String atr2) throws Exception {
    if (isBlank(atr1) || isBlank(atr2)) {
      return 0;
    }

    final Map<String, Integer> bow1 = createBagOfWords(atr1);
    final Map<String, Integer> bow2 = createBagOfWords(atr2);
    final Set<String> atr1Tokens = bow1.keySet();
    final Set<String> atr2Tokens = bow2.keySet();
    final List<String> tokens = join(atr1Tokens, atr2Tokens);

    Map<String, Tuple3<Double, Double, Double>> close = new HashMap<>();

    tokens.forEach(s -> {
      double tfidf1 = tfidf(bow1, s);
      double tfidf2 = tfidf(bow2, s);
      close.put(s, Tuple3.of(tfidf1, tfidf2, 0d));
    });

    for (String s : atr1Tokens) {
      final Tuple3<Double, Double, Double> old = close.get(s);
      if (atr2Tokens.contains(s)) {
        old.f2 = 1d;
      } else {
        for (String t : atr2Tokens) {
          final double sim = similarityMeasurer.computeSimilarity(s, t);
          if (sim > threshold && old.f2 < sim) {
            old.f0 = tfidf(bow1, s, t);
            old.f1 = tfidf(bow2, t, s);
            old.f2 = sim;
          }
        }
      }
    }

    return computeWeightedCosineSimilarity(close.values());
  }

  private List<String> join(Collection<String> first, Collection<String> second) {
    final Set<String> result = new HashSet<>(first);
    result.addAll(second);
    return new ArrayList<>(result);
  }

  private double tfidf(final Map<String, Integer> doc, final String... words) {
    return Math.log(1.0 + tf(doc, words)) * idf(words);
  }

  /**
   * Calculates the sum of all tf frequencies
   *
   * @param bow   bag of words with term frequencies
   * @param terms terms to get their frequencies of
   * @return summed term frequencies
   */
  private long tf(final Map<String, Integer> bow, final String... terms) {
    long count = 0L;
    for (String term : terms) {
      count += bow.getOrDefault(term, 0);
    }
    return count;
  }

  /**
   * Computes the cosine similarity of two vectors with a given weight
   *
   * @param values 3-tuple with the vector components and the weight
   * @return the wighted cosine similarity of the two vectors
   */
  private double computeWeightedCosineSimilarity(Collection<Tuple3<Double, Double, Double>> values) {
    double ab = 0;
    double norm1 = 0;
    double norm2 = 0;

    for (Tuple3<Double, Double, Double> token : values) {
      ab += token.f0 * token.f1 * token.f2;
      norm1 += token.f0 * token.f0;
      norm2 += token.f1 * token.f1;
    }
    final double sim = ab / (Math.sqrt(norm1) * Math.sqrt(norm2));
    // min due to precision error, might otherwise return 1.0000000001
    return Math.min(sim, 1);
  }
}
