package org.gradoop.famer.linking.similarityMeasuring.methods.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.common.Utils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Collect all words from a property and assigns the to the graph id
 */
public class TokenizeAttribute implements FlatMapFunction<EPGMVertex, Tuple2<GradoopId, String>> {

  private final List<String> attributes;
  private final String tokenizer;

  public TokenizeAttribute(List<String> attributes, String tokenizer) {
    this.attributes = attributes;
    this.tokenizer = tokenizer;
  }

  @Override
  public void flatMap(EPGMVertex value, Collector<Tuple2<GradoopId, String>> out) throws Exception {
    for (String attribute : attributes) {
      if (value.hasProperty(attribute)) {
        final String document = Utils.cleanAttributeValue(value.getPropertyValue(attribute).getString());

        final HashSet<String> words = new HashSet<>(Arrays.asList(document.split(tokenizer)));
        for (String string : words) {
          out.collect(Tuple2.of(value.getId(), string));
        }
      }
    }
  }
}
