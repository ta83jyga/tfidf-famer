package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;
import org.gradoop.famer.linking.similarityMeasuring.methods.SoftTFIDF;

import java.util.Map;

public class SoftTFIDFSimilarityComponent extends SimilarityComponent implements
  TFIDFBasedSimilarityComponent {
  /**
   * Word tokenizer
   */
  private final String tokenizer;

  /**
   * Threshold for first level similarity measurement
   */
  private final double threshold;

  /**
   * Pre-calculated document frequencies of each word
   */
  private Map<String, Integer> documentFrequency;

  /**
   * Total amount of documents
   */
  private long documentsCount;

  /**
   * Creates an instance of TFIDFSimilarityComponent
   *
   * @param baseConfig The base configuration for the similarity component
   * @param tokenizer  Word tokenizer
   * @param threshold  the tfidf variant
   */
  public SoftTFIDFSimilarityComponent(SimilarityComponentBaseConfig baseConfig, String tokenizer,
    double threshold) {
    super(baseConfig);
    this.tokenizer = tokenizer;
    this.threshold = threshold;
  }

  @Override
  public SimilarityComputation<String> buildSimilarityComputation() throws Exception {
    return new SoftTFIDF(this.documentFrequency, tokenizer, documentsCount, threshold);
  }

  public void setDocumentFrequency(Map<String, Integer> documentFrequency) {
    this.documentFrequency = documentFrequency;
  }

  public void setDocumentsCount(long documentsCount) {
    this.documentsCount = documentsCount;
  }
}
