package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.famer.linking.similarityMeasuring.dataStructures.TFIDFVariant;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.apache.commons.lang.StringUtils.isBlank;

public class TFIDF extends TFIDFBasedSimilarityComputation {

  /**
   * TFIDF variant to use
   */
  private final TFIDFVariant tfidfVariant;

  /**
   * Coefficient used for augmented tfidf
   */
  private final double atCoefficient;

  /**
   * @param documentFrequency a mapping of the document frequency to each word
   * @param tokenizer         sentence tokenizer
   * @param documentsCount    total amount of documents
   * @param tfidfVariant      TFIDF variant to use
   */
  public TFIDF(final Map<String, Integer> documentFrequency, final String tokenizer, long documentsCount,
    TFIDFVariant tfidfVariant) {
    super(tokenizer, documentFrequency, documentsCount);
    this.tfidfVariant = tfidfVariant;
    this.atCoefficient = 0.4;
  }

  /**
   * @param documentFrequency a mapping of the document frequency to each word
   * @param tokenizer         sentence tokenizer
   * @param documentsCount    total amount of documents
   * @param tfidfVariant      TFIDF variant to use
   * @param atCoefficient     coefficient used for augmented tfidf
   */
  public TFIDF(final Map<String, Integer> documentFrequency, final String tokenizer, long documentsCount,
    TFIDFVariant tfidfVariant, double atCoefficient) {
    super(tokenizer, documentFrequency, documentsCount);
    this.tfidfVariant = tfidfVariant;
    this.atCoefficient = atCoefficient;
  }


  @Override
  public double computeSimilarity(String atr1, String atr2) throws Exception {
    Objects.requireNonNull(atr1, "First attribute should not be null");
    Objects.requireNonNull(atr2, "Second attribute should not be null");
    if (isBlank(atr1) || isBlank(atr2)) {
      return 0;
    }
    final Map<String, Integer> bow1 = createBagOfWords(atr1);
    final Map<String, Integer> bow2 = createBagOfWords(atr2);

    final Map<String, Double> doc1Weights = tfidf(bow1);
    final Map<String, Double> doc2Weights = tfidf(bow2);

    final double norm1 = computeVectorNorm(doc1Weights.values());
    final double norm2 = computeVectorNorm(doc2Weights.values());

    double sim = 0;
    for (Map.Entry<String, Double> term : doc1Weights.entrySet()) {
      if (doc2Weights.containsKey(term.getKey())) {
        sim += term.getValue() * doc2Weights.get(term.getKey()) / (norm1 * norm2);
      }
    }
    // min due to precision error, might otherwise return 1.0000000001
    return Math.min(sim, 1);
  }

  /**
   * @param bow bag of word oh an attribute with term frequencies
   * @return tfidf score
   */
  private Map<String, Double> tfidf(final Map<String, Integer> bow) {
    final Map<String, Double> result = new HashMap<>();

    final int max = Collections.max(bow.values());
    for (Map.Entry<String, Integer> term : bow.entrySet()) {
      switch (tfidfVariant) {
      case N_T:
        final double ntfIdf = term.getValue() * idf(term.getKey());
        result.put(term.getKey(), ntfIdf);
        break;
      case L_T:
        final double ltfIdf = Math.log(1.0 + term.getValue()) * idf(term.getKey());
        result.put(term.getKey(), ltfIdf);
        break;
      case A_T:
        final double atfIdf =
          (atCoefficient + ((1 - atCoefficient) * term.getValue() / max)) * idf(term.getKey());
        result.put(term.getKey(), atfIdf);
        break;
      case B_T:
        result.put(term.getKey(), idf(term.getKey()));
        break;
      }
    }
    return result;
  }

  /**
   * Computes the norm of a vector
   *
   * @param vector a vector
   * @return the norm of this vector
   */
  private double computeVectorNorm(final Collection<Double> vector) {
    double sum = 0;
    for (Double aFloat : vector) {
      sum += aFloat * aFloat;
    }
    return Math.sqrt(sum);
  }
}
