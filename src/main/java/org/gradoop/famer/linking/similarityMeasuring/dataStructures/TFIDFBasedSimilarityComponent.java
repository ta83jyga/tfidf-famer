package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

import java.util.Map;

public interface TFIDFBasedSimilarityComponent {
  /**
   * Broadcast variable used to set the pre-calculated document frequency map
   */
  String DOCUMENT_FREQUENCY_BROADCAST_VARIABLE = "documentFrequency";
  /**
   * Broadcast variable used to set the total amount of documents
   */
  String DOCUMENTS_COUNT_BROADCAST_VARIABLE = "documentsCount";

  SimilarityComputation<String> buildSimilarityComputation() throws Exception;

  void setDocumentFrequency(Map<String, Integer> documentFrequency);

  void setDocumentsCount(long documentsCount);
}
