package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;
import org.gradoop.famer.linking.similarityMeasuring.methods.TFIDF;

import java.util.Map;

public class TFIDFSimilarityComponent extends SimilarityComponent implements TFIDFBasedSimilarityComponent {
  /**
   * Word tokenizer
   */
  private final String tokenizer;

  /**
   * TFIDF variant to use
   */
  private final TFIDFVariant tfidfVariant;

  private final double atCoefficient;

  /**
   * Pre-calculated document frequencies of each word
   */
  private Map<String, Integer> documentFrequency;

  /**
   * Total amount of documents
   */
  private long documentsCount;

  /**
   * Creates an instance of TFIDFSimilarityComponent
   *
   * @param baseConfig   The base configuration for the similarity component
   * @param tokenizer    Word tokenizer
   * @param tfidfVariant the tfidf variant
   */
  public TFIDFSimilarityComponent(SimilarityComponentBaseConfig baseConfig, String tokenizer,
    TFIDFVariant tfidfVariant) {
    super(baseConfig);
    this.tokenizer = tokenizer;
    this.tfidfVariant = tfidfVariant;
    this.atCoefficient = 0.5;
  }

  /**
   * Creates an instance of TFIDFSimilarityComponent
   *
   * @param baseConfig    The base configuration for the similarity component
   * @param tokenizer     Word tokenizer
   * @param tfidfVariant  the tfidf variant
   * @param atCoefficient coefficient used for augmented tfidf
   */
  public TFIDFSimilarityComponent(SimilarityComponentBaseConfig baseConfig, String tokenizer,
    TFIDFVariant tfidfVariant, double atCoefficient) {
    super(baseConfig);
    this.tokenizer = tokenizer;
    this.tfidfVariant = tfidfVariant;
    this.atCoefficient = atCoefficient;
  }

  @Override
  public SimilarityComputation<String> buildSimilarityComputation() throws Exception {
    return new TFIDF(this.documentFrequency, tokenizer, documentsCount, tfidfVariant, atCoefficient);
  }

  @Override
  public void setDocumentFrequency(Map<String, Integer> documentFrequency) {
    this.documentFrequency = documentFrequency;
  }

  @Override
  public void setDocumentsCount(long documentsCount) {
    this.documentsCount = documentsCount;
  }
}
