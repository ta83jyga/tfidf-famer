package org.gradoop.famer.linking.similarityMeasuring.methods.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;

/**
 * Counts the size of the group consisting of a word and all the documents it appears
 */
public class CountDocumentFrequency implements
  GroupReduceFunction<Tuple2<GradoopId, String>, Tuple2<String, Integer>> {

  @Override
  public void reduce(Iterable<Tuple2<GradoopId, String>> group, Collector<Tuple2<String, Integer>> out) throws
    Exception {
    String word = "";
    int count = 0;
    for (Tuple2<GradoopId, String> v : group) {
      word = v.f1;
      count++;
    }
    out.collect(Tuple2.of(word, count));
  }
}
