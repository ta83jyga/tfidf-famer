package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public abstract class TFIDFBasedSimilarityComputation implements SimilarityComputation<String> {
  /**
   * Tokenizer to split sentences in words
   */
  protected final String tokenizer;
  /**
   * Map holding the document frequency of each word
   */
  protected final Map<String, Integer> documentFrequency;
  /**
   * Total amount of documents
   */
  protected final long documentsCount;

  public TFIDFBasedSimilarityComputation(String tokenizer, Map<String, Integer> documentFrequency,
    long documentsCount) {
    this.tokenizer = tokenizer;
    this.documentFrequency = documentFrequency;
    this.documentsCount = documentsCount;
  }

  public String parsePropertyValue(PropertyValue value) {
    return getString(value);
  }

  /**
   * Count term frequencies and creates bag of words
   *
   * @param document the document
   * @return bag of words
   */
  protected Map<String, Integer> createBagOfWords(String document) {
    final Map<String, Integer> bow = new HashMap<>();
    for (String string : document.split(this.tokenizer)) {
      bow.compute(string, (k, v) -> v == null ? 1 : v + 1);
    }
    return bow;
  }

  /**
   * Calculate the inverse document frequency after  1 + log(|D| / |{d:ti ∈ d}|)
   *
   * @param term the term
   * @return the inverse document frequency
   */
  protected double idf(final String... terms) {
    long df = 0;
    try {
      for (String term : terms) {
        df = documentFrequency.get(term);
      }
    } catch (NullPointerException e) {
      throw new IllegalStateException(
        "Could not get document frequency for " + Arrays.toString(terms) + ". " +
          "Did you include all attributes for pre calculating the document frequency?");
    }
    if (df == 0) {
      return 0D;
    }
    return 1 + Math.log(documentsCount / (double) df);
  }
}
