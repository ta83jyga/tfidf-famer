package org.gradoop.famer.linking.similarityMeasuring;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.EmbeddingComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.TFIDFBasedSimilarityComponent;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This is just a modification to broadcast the TFIDF DocumentFrequency
 * should be later placed in {@link SimilarityMeasurer}
 */
public class SimilarityMeasurerWithDocumentFrequencyBroadcast extends SimilarityMeasurer {

  /**
   * All similarity components of the matching process
   */
  private final List<SimilarityComponent> similarityComponents;

  /**
   * Creates an instance of SimilarityMeasurer
   *
   * @param similarityComponents All similarity components of the matching process
   */
  public SimilarityMeasurerWithDocumentFrequencyBroadcast(List<SimilarityComponent> similarityComponents) {
    super(similarityComponents);
    this.similarityComponents = similarityComponents;
  }

  /**
   * Get word vector files registered in Flinks distributed cache
   *
   * @param parameters The configuration parameters
   */
  @Override
  public void open(Configuration parameters) throws Exception {
    for (SimilarityComponent c : similarityComponents) {
      if (c instanceof EmbeddingComponent) {
        EmbeddingComponent component = (EmbeddingComponent) c;
        File vectorFile =
          getRuntimeContext().getDistributedCache().getFile(component.getDistributedCacheKey());
        component.setWordVectorsFile(vectorFile);
      } else if (c instanceof TFIDFBasedSimilarityComponent) {
        final TFIDFBasedSimilarityComponent component = (TFIDFBasedSimilarityComponent) c;

        List<Tuple2<String, Integer>> dfTupel = getRuntimeContext()
          .getBroadcastVariable(TFIDFBasedSimilarityComponent.DOCUMENT_FREQUENCY_BROADCAST_VARIABLE);

        final Map<String, Integer> df =
          dfTupel.stream().collect(Collectors.toMap(token -> token.f0, count -> count.f1));

        (component).setDocumentFrequency(df);

        List<Long> documentsCount = getRuntimeContext()
          .getBroadcastVariable(TFIDFBasedSimilarityComponent.DOCUMENTS_COUNT_BROADCAST_VARIABLE);
        (component).setDocumentsCount(documentsCount.get(0));
      }
    }
  }
}
