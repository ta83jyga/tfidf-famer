package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

/**
 * TFIDF term weighting variants after with a simplified SMART notation
 * First letter is for term frequency weight, second letter for document frequency weight
 */
public enum TFIDFVariant {

  /**
   * TF: row term count  (natural)
   * IDF: logarithmic inverse collection frequency
   */
  N_T,

  /**
   * TF: logarithmic
   * IDF: logarithmic inverse collection frequency
   */
  L_T,

  /**
   * TF: augmented normalized
   * IDF: logarithmic inverse collection frequency
   */
  A_T,

  /**
   * TF: binary normalized
   * IDF: logarithmic inverse collection frequency
   * sort of mainly IDF
   */
  B_T;
}
