package org.gradoop.famer;

import org.apache.flink.api.java.utils.ParameterTool;
import org.gradoop.famer.evaluation.AbtBuyEvaluation;
import org.gradoop.famer.evaluation.AmazonGoogleEvaluation;
import org.gradoop.famer.evaluation.CameraEvaluation;
import org.gradoop.famer.evaluation.Evaluation;

/**
 * Starts the evaluations
 */
public class Main {
  public static void main(String[] args) throws Exception {
    final ParameterTool params = ParameterTool.fromArgs(args);
    final String dataset = params.get("dataset");
    final boolean speedUp = params.getBoolean("speedup", false);

    Evaluation evaluation;
    switch (dataset) {
    case "ab":
      evaluation = new AbtBuyEvaluation(args);
      break;
    case "ag":
      evaluation = new AmazonGoogleEvaluation(args);
      break;
    case "pm":
      evaluation = new CameraEvaluation("priceme", args);
      break;
    case "gs":
      evaluation = new CameraEvaluation("gosale", args);
      break;
    case "pg":
      evaluation = new CameraEvaluation("priceme", "gosale", args);
      break;
    default:
      throw new IllegalArgumentException("Unsupported source " + dataset);
    }
    if (speedUp) {
      evaluation.runSpeedUp();
    } else {
      evaluation.runEvaluation();
    }
  }
}
