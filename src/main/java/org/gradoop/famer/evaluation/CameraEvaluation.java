package org.gradoop.famer.evaluation;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.parallelClustering.star.Star;
import org.gradoop.famer.linking.blocking.BlockMaker;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GoldenTruthFileType;
import org.gradoop.famer.preprocessing.io.benchmarks.CameraReader;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import static org.gradoop.famer.linking.blocking.methods.dataStructures.StandardBlockingEmptyKeyStrategy.ADD_TO_ALL;

/**
 * Evaluation for Camera-Dataset
 */
public class CameraEvaluation extends EvaluationBase {
  private final CameraReader reader;

  public CameraEvaluation(String source, String[] args) {
    super(source, source, GoldenTruthFileType.ENTITY_ID_GT_ID_PAIR, args);
    reader = new CameraReader(source);
  }

  public CameraEvaluation(String source1, String source2, String[] args) {
    super(source1, source2, GoldenTruthFileType.ENTITY_ID_GT_ID_PAIR, args);
    reader = new CameraReader(source1, source2);
  }

  @Override
  public void runEvaluation() throws Exception {
    run(reader, 1.0);
  }

  @Override
  public void runSpeedUp() throws Exception {
    runSpeedUp(reader);
  }

  @Override
  protected LogicalGraph runClustering(LogicalGraph linkingResult) {
    return new Star(PrioritySelection.MAX, Star.StarType.TWO, false, ClusteringOutputType.GRAPH_COLLECTION,
      Integer.MAX_VALUE).execute(linkingResult);
  }

  @Override
  protected DataSet<Tuple2<EPGMVertex, EPGMVertex>> runBlocking(DataSet<EPGMVertex> vertices) {
    BlockMaker blockMaker = getStandardPrefixBlockMaker("brand", ADD_TO_ALL, 3);
    return blockMaker.execute(vertices);
  }
}
