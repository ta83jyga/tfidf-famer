package org.gradoop.famer.evaluation;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.clip.CLIP;
import org.gradoop.famer.clustering.parallelClustering.clip.dataStructures.CLIPConfig;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.linking.blocking.BlockMaker;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GoldenTruthFileType;
import org.gradoop.famer.preprocessing.io.benchmarks.abtbuy.AbtBuyReader;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import static org.gradoop.famer.linking.blocking.methods.dataStructures.StandardBlockingEmptyKeyStrategy.ADD_TO_ALL;

/**
 * Evaluation for AbtBuy-Dataset
 */
public class AbtBuyEvaluation extends EvaluationBase {
  public AbtBuyEvaluation(String[] args) {
    super("Abt", "Buy", GoldenTruthFileType.MATCHED_PAIRS, args);
  }

  @Override
  public void runEvaluation() throws Exception {
    final AbtBuyReader reader = new AbtBuyReader();
    run(reader, 0.3);
  }

  @Override
  public void runSpeedUp() throws Exception {
    final AbtBuyReader reader = new AbtBuyReader();
    runSpeedUp(reader);
  }

  @Override
  protected LogicalGraph runClustering(LogicalGraph linkingResult) {
    return new CLIP(new CLIPConfig(), ClusteringOutputType.GRAPH_COLLECTION, Integer.MAX_VALUE)
      .execute(linkingResult);
  }

  @Override
  protected DataSet<Tuple2<EPGMVertex, EPGMVertex>> runBlocking(DataSet<EPGMVertex> vertices) {
    BlockMaker blockMaker = getStandardPrefixBlockMaker("name", ADD_TO_ALL);
    return blockMaker.execute(vertices);
  }
}
