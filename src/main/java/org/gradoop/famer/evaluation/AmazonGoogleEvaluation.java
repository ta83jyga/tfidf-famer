package org.gradoop.famer.evaluation;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.clip.CLIP;
import org.gradoop.famer.clustering.parallelClustering.clip.dataStructures.CLIPConfig;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.linking.blocking.BlockMaker;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GoldenTruthFileType;
import org.gradoop.famer.preprocessing.io.benchmarks.amazon.AmazonProductsReader;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import static org.gradoop.famer.linking.blocking.methods.dataStructures.StandardBlockingEmptyKeyStrategy.ADD_TO_ALL;
import static org.gradoop.famer.linking.blocking.methods.dataStructures.StandardBlockingEmptyKeyStrategy.REMOVE;

/**
 * Evaluation for AmazonGoogle-Dataset
 */
public class AmazonGoogleEvaluation extends EvaluationBase {
  public AmazonGoogleEvaluation(String[] args) {
    super("Amazon", "Google", GoldenTruthFileType.MATCHED_PAIRS, args);
  }

  @Override
  public void runEvaluation() throws Exception {
    final AmazonProductsReader amazonProductsReader = new AmazonProductsReader();
    run(amazonProductsReader, 0.3);
  }

  @Override
  public void runSpeedUp() throws Exception {
    final AmazonProductsReader amazonProductsReader = new AmazonProductsReader();
    runSpeedUp(amazonProductsReader);
  }

  @Override
  protected LogicalGraph runClustering(LogicalGraph linkingResult) {
    return new CLIP(new CLIPConfig(), ClusteringOutputType.GRAPH_COLLECTION, Integer.MAX_VALUE)
      .execute(linkingResult);
  }

  @Override
  protected DataSet<Tuple2<EPGMVertex, EPGMVertex>> runBlocking(DataSet<EPGMVertex> vertices) {
    BlockMaker blockMaker = getStandardPrefixBlockMaker("title", ADD_TO_ALL);
    BlockMaker blockMaker2 = getStandardPrefixBlockMaker("manufacturer", REMOVE);
    BlockMaker blockMaker3 = getStandardPrefixBlockMaker("tm", REMOVE);

    return blockMaker.execute(vertices).union(blockMaker2.execute(vertices))
      .union(blockMaker3.execute(vertices));
  }
}
