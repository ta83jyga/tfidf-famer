package org.gradoop.famer.evaluation;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.io.FileUtils;
import org.apache.flink.api.common.JobExecutionResult;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.io.DiscardingOutputFormat;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.utils.ParameterTool;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMEdgeFactory;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.BlockMaker;
import org.gradoop.famer.linking.blocking.keyGeneration.BlockingKeyGenerator;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.KeyGeneratorComponent;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.PrefixLengthComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponentBaseConfig;
import org.gradoop.famer.linking.blocking.methods.dataStructures.StandardBlockingComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.StandardBlockingEmptyKeyStrategy;
import org.gradoop.famer.linking.linking.dataStructures.LinkerComponent;
import org.gradoop.famer.linking.linking.functions.LinkMaker;
import org.gradoop.famer.linking.selection.Selector;
import org.gradoop.famer.linking.selection.dataStructures.SelectionComponent;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.Condition;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.ConditionOperator;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRule;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRuleComponent;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRuleComponentType;
import org.gradoop.famer.linking.similarityMeasuring.SimilarityMeasurer;
import org.gradoop.famer.linking.similarityMeasuring.SimilarityMeasurerWithDocumentFrequencyBroadcast;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.*;
import org.gradoop.famer.linking.similarityMeasuring.methods.functions.CountDocumentFrequency;
import org.gradoop.famer.linking.similarityMeasuring.methods.functions.TokenizeAttribute;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.ClusteringQualityWithGTFile;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GTFileComponent;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GoldenTruthFileType;
import org.gradoop.famer.preprocessing.io.benchmarks.BenchmarkSetReaderBase;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.operators.count.Count;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.apache.commons.math3.util.Precision.round;
import static org.gradoop.famer.linking.linking.Linker.SIM_VALUE;
import static org.gradoop.famer.linking.similarityMeasuring.dataStructures.TFIDFBasedSimilarityComponent.DOCUMENTS_COUNT_BROADCAST_VARIABLE;
import static org.gradoop.famer.linking.similarityMeasuring.dataStructures.TFIDFBasedSimilarityComponent.DOCUMENT_FREQUENCY_BROADCAST_VARIABLE;

/**
 * Base Evaluation Class
 */
public abstract class EvaluationBase implements Evaluation {
  private static final String TOKENIZER = "\\s+|[\\]\\[(){}]";
  private static final String COMPONENT_ID = "id";
  private final String sourceGraph;
  private final String targetGraph;
  private final GoldenTruthFileType goldenTruthFileType;
  private final int parallelism;
  private final String method;
  private final String dataset;
  private final String attribute;
  private final double coefficient;
  private final String datasetPath;

  protected EvaluationBase(String sourceGraph, String targetGraph, GoldenTruthFileType goldenTruthFileType,
    String... args) {
    this.sourceGraph = sourceGraph;
    this.targetGraph = targetGraph;
    this.goldenTruthFileType = goldenTruthFileType;
    final ParameterTool params = ParameterTool.fromArgs(args);
    this.method = params.get("method", "lt");
    String data = params.get("data", "data");
    this.dataset = params.get("dataset");
    this.parallelism = params.getInt("p", 10);
    this.attribute = params.get("attr", "title");
    this.coefficient = Double.parseDouble(params.get("coef", "0.9"));
    this.datasetPath = data + File.separator + dataset;
  }

  @SuppressWarnings("rawtypes")
  protected void run(BenchmarkSetReaderBase benchmarkReader, double maxThreshold) throws Exception {
    final GraphCollection benchmarkData = benchmarkReader.getBenchmarkDataAsGraphCollection(datasetPath);
    final ExecutionEnvironment env = benchmarkData.getConfig().getExecutionEnvironment();
    env.setParallelism(parallelism);

    File file = new File(
      String.format("output/%s/%s/%s/%s-%s-%s", dataset, attribute, method, method, attribute, coefficient)
        .replace("/", File.separator));
    System.out.println(file);
    FileUtils.writeStringToFile(file,
      "threshold,precision,recall,fMeasure,allPositive,truePositive,perfectCompleteClusterNo");

    /* Blocking */
    final DataSet<EPGMVertex> vertices = benchmarkData.getVertices();
    DataSet<Tuple2<EPGMVertex, EPGMVertex>> blockedVertices = runBlocking(vertices);

    /* SIMILARITY MEASURING */
    DataSet<Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>> similarityFields =
      measureSimilarity(method, attribute, coefficient, benchmarkData, blockedVertices);

    for (double threshold = 0.1; threshold <= maxThreshold; threshold = round(threshold + 0.1, 1)) {
      /* SELECTION */
      final LogicalGraph linkingResult = selection(benchmarkData, vertices, similarityFields, threshold);

      /* CLUSTERING */
      final LogicalGraph graphCollection = runClustering(linkingResult);

      /* PERFORMANCE MEASUREMENT */
      measurePerformance(datasetPath, file, threshold, graphCollection);
    }
  }

  @SuppressWarnings("rawtypes")
  public void runSpeedUp(BenchmarkSetReaderBase benchmarkReader) throws Exception {

    final GraphCollection benchmarkData = benchmarkReader.getBenchmarkDataAsGraphCollection(datasetPath);
    final ExecutionEnvironment env = benchmarkData.getConfig().getExecutionEnvironment();
    env.setParallelism(parallelism);

    /* Blocking */
    final DataSet<EPGMVertex> vertices = benchmarkData.getVertices();
    DataSet<Tuple2<EPGMVertex, EPGMVertex>> blockedVertices = runBlocking(vertices);

    /* SIMILARITY MEASURING */
    DataSet<Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>> similarityFields =
      measureSimilarity(method, attribute, 0.9, benchmarkData, blockedVertices);

    /* WRITE TO DATA_SINK */
    similarityFields.output(new DiscardingOutputFormat<>());

    final JobExecutionResult executionResult = env.execute("SPEED-UP " + method + " par " + parallelism);
    final long netRuntime = executionResult.getNetRuntime();
    File file = new File("output/" + dataset + "/speedup.csv");
    FileUtils
      .writeStringToFile(file, method + "," + parallelism + "," + netRuntime + "\n", StandardCharsets.UTF_8,
        true);
  }

  abstract LogicalGraph runClustering(LogicalGraph linkingResult);

  abstract DataSet<Tuple2<EPGMVertex, EPGMVertex>> runBlocking(DataSet<EPGMVertex> vertices);

  private DataSet<Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>> measureSimilarity(String method,
    String attribute, double coefficient, GraphCollection benchmarkData,
    DataSet<Tuple2<EPGMVertex, EPGMVertex>> blockedVertices) {
    final DataSet<Long> documentsCount = Count.count(benchmarkData.getVertices());

    /* Build wordCountDict (documentFrequencies) */
    DataSet<Tuple2<String, Integer>> documentFrequencies = benchmarkData.getVertices()
      .flatMap(new TokenizeAttribute(Collections.singletonList(attribute), TOKENIZER)).groupBy(1)
      .reduceGroup(new CountDocumentFrequency());

    /* SIMILARITY MEASURING */
    final SimilarityComponentBaseConfig baseConfig =
      new SimilarityComponentBaseConfig(COMPONENT_ID, sourceGraph, sourceGraph, attribute, targetGraph,
        targetGraph, attribute, 1d);

    // @formatter:off
    SimilarityComponent similarityComponent  =
      new ImmutableMap.Builder<String, SimilarityComponent>()
        .put("ed", new EditDistanceComponent(baseConfig))
        .put("jw", new JaroWinklerComponent(baseConfig, coefficient))
        .put("lt", new TFIDFSimilarityComponent(baseConfig, TOKENIZER, TFIDFVariant.L_T))
        .put("at", new TFIDFSimilarityComponent(baseConfig, TOKENIZER, TFIDFVariant.A_T, coefficient))
        .put("nt", new TFIDFSimilarityComponent(baseConfig, TOKENIZER, TFIDFVariant.N_T))
        .put("soft", new SoftTFIDFSimilarityComponent(baseConfig, TOKENIZER, coefficient))
        .put("mej", new MongeElkanComponent(baseConfig, TOKENIZER, coefficient))
        .put("ej", new ExtendedJaccardComponent(baseConfig, TOKENIZER, coefficient, 0.7))
        .build().get(method);
    // @formatter:on

    final DataSet<Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>> similarityFields;
    List<SimilarityComponent> similarityComponents = Collections.singletonList(similarityComponent);
    if (similarityComponent instanceof TFIDFBasedSimilarityComponent) {
      similarityFields =
        blockedVertices.flatMap(new SimilarityMeasurerWithDocumentFrequencyBroadcast(similarityComponents))
          .withBroadcastSet(documentFrequencies, DOCUMENT_FREQUENCY_BROADCAST_VARIABLE)
          .withBroadcastSet(documentsCount, DOCUMENTS_COUNT_BROADCAST_VARIABLE);
    } else {
      similarityFields = blockedVertices.flatMap(new SimilarityMeasurer(similarityComponents));
    }
    return similarityFields;
  }

  private LogicalGraph selection(GraphCollection benchmarkDataCollection, DataSet<EPGMVertex> vertices,
    DataSet<Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>> similarityFields, double threshold) {
    /* SELECTION */
    final LinkerComponent linkerComponent = new LinkerComponent();
    linkerComponent.setSelectionComponent(createSelectorComponent(threshold));

    DataSet<Tuple3<EPGMVertex, EPGMVertex, Double>> similarityDegree =
      similarityFields.flatMap(new Selector(linkerComponent));

    /* POST-PROCESSING */
    DataSet<EPGMEdge> edges =
      similarityDegree.map(new LinkMaker(new EPGMEdgeFactory(), SIM_VALUE, false, false));

    return benchmarkDataCollection.getConfig().getLogicalGraphFactory().fromDataSets(vertices, edges);
  }

  private void measurePerformance(String data, File file, double threshold,
    LogicalGraph graphCollection) throws Exception {
    String goldenTruthFilePath = data + "/PerfectMapping.csv";
    GTFileComponent gtFileComponent = new GTFileComponent(goldenTruthFilePath, ",", this.goldenTruthFileType);
    ClusteringQualityWithGTFile qualityMeasures =
      new ClusteringQualityWithGTFile(graphCollection.getVertices(), gtFileComponent, "id", true);

    qualityMeasures.computeQuality();
    final String result = String.format("%s,%s,%s,%s,%s,%s,%s", threshold, qualityMeasures.computePrecision(),
      qualityMeasures.computeRecall(), qualityMeasures.computeFMeasure(), qualityMeasures.getAllPositives(),
      qualityMeasures.getTruePositives(), qualityMeasures.getGtRecordNo());

    System.out.println("result = " + result);
    FileUtils.writeStringToFile(file, "\n" + result, StandardCharsets.UTF_8, true);
  }

  protected BlockMaker getStandardPrefixBlockMaker(String attribute,
    StandardBlockingEmptyKeyStrategy emptyKeyStrategy) {
    return getStandardPrefixBlockMaker(attribute, emptyKeyStrategy, 1);
  }

  protected BlockMaker getStandardPrefixBlockMaker(String attribute,
    StandardBlockingEmptyKeyStrategy emptyKeyStrategy, int len) {
    KeyGeneratorComponent keyGeneratorComponent = new PrefixLengthComponent(attribute, len);

    BlockingKeyGenerator blockingKeyGenerator = new BlockingKeyGenerator(keyGeneratorComponent);
    Map<String, Set<String>> graphPairs = new HashMap<>();
    // g1 is limited to g2
    Set<String> value1 = new HashSet<>();
    value1.add(this.targetGraph);
    graphPairs.put(this.sourceGraph, value1);
    // g2 is limited to g1
    Set<String> value2 = new HashSet<>();
    value2.add(this.sourceGraph);
    graphPairs.put(this.targetGraph, value2);
    Set<String> values = new HashSet<>();
    values.add("*");
    Map<String, Set<String>> categoryPairs = new HashMap<>();
    categoryPairs.put("*", values);
    BlockingComponentBaseConfig blockingBaseConfig =
      new BlockingComponentBaseConfig(blockingKeyGenerator, graphPairs, categoryPairs);
    BlockingComponent blockingComponent =
      new StandardBlockingComponent(blockingBaseConfig, parallelism, emptyKeyStrategy);
    return new BlockMaker(blockingComponent);
  }

  protected SelectionComponent createSelectorComponent(double greatEqual) {
    Condition less = new Condition("smallerEqual", COMPONENT_ID, ConditionOperator.SMALLER_EQUAL, 1);
    Condition greater =
      new Condition("greaterEqual", COMPONENT_ID, ConditionOperator.GREATER_EQUAL, greatEqual);

    SelectionRuleComponent andComp =
      new SelectionRuleComponent(SelectionRuleComponentType.SELECTION_OPERATOR, "AND");
    SelectionRuleComponent lessComp =
      new SelectionRuleComponent(SelectionRuleComponentType.CONDITION, "smallerEqual");
    SelectionRuleComponent greatComp =
      new SelectionRuleComponent(SelectionRuleComponentType.CONDITION, "greaterEqual");

    List<Condition> conditions = Arrays.asList(less, greater);
    List<SelectionRuleComponent> selectionRuleComponents = Arrays.asList(lessComp, andComp, greatComp);
    SelectionRule selectionRule = new SelectionRule(conditions, selectionRuleComponents);
    return new SelectionComponent(selectionRule);
  }
}
