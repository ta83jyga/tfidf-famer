package org.gradoop.famer.evaluation;

public interface Evaluation {
  void runEvaluation() throws Exception;

  void runSpeedUp() throws Exception;
}
