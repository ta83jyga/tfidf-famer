"""
Contains functions for plotting the evaluation results
"""
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import matplotlib
import numpy as np

sns.set_theme(style='whitegrid')


def comparison_plot(name: str, x_ax: str = 'Datensatz', y_ax: str = 'F-Measure', hue: str = 'Methode'):
    """
    Compare several results using bars
    :param name: result file name
    :param x_ax: csv column to use as x axes
    :param y_ax: csv column to use as y axes
    :param hue: compared data
    """
    data = pd.read_csv(f'final/{name}.csv')
    g = sns.catplot(
        data=data, kind='bar',
        x=x_ax, y=y_ax, hue=hue,
        palette='deep', alpha=.9, height=9,
        legend=None
    )

    legend = plt.legend(loc='upper right')
    legend.set_title(hue)
    plt.tight_layout()

    fig = g.fig
    fig.set_size_inches(w=5.70045, h=3.50045)
    fig.savefig(f'fig/{name}.pgf', bbox_inches='tight')


def line_plot(name: str, x_ax: str = 'Schellwert', y_ax: str = 'F-Measure', hue: str = 'Datensatz'):
    """
    Line plot
    :param name: result file name
    :param x_ax: csv column to use as x axes
    :param y_ax: csv column to use as y axes
    :param hue: compared data
    """
    data = pd.read_csv(f'final/{name}.csv')
    g = sns.lineplot(
        data=data,
        x=x_ax, y=y_ax, hue=hue,
        palette='deep', alpha=.9,
        legend=None,
    )
    g.set_xlim(0, 1)
    plt.tight_layout()

    fig = g.get_figure()
    fig.set_size_inches(w=4.30045, h=3.90045)
    fig.savefig(f'fig/{name}.pgf', bbox_inches='tight')


def speedup_plot(name: str = 'speedup'):
    """
    Plot speedup curve
    :param name: speedup result file name
    """
    data = pd.read_csv(f'final/{name}.csv')
    data['time'] = data['time'] / 1000
    data['Speedup'] = data.groupby('Methode').time.transform(lambda x: x.max() / x)

    g = sns.lineplot(
        data=data,
        x='Worker', y='Speedup', hue='Methode',
        palette='deep', alpha=.9,
    )
    g.set_xlim(1, 16)
    g.set_ylim(1, 10)
    plt.legend(loc='upper left')

    # add ideal speedup
    plt.plot(np.linspace(0, 16, 100), np.linspace(0, 16, 100), color='grey', linestyle='dotted', linewidth=1)
    plt.tight_layout()

    fig = g.get_figure()
    fig.set_size_inches(w=5.00045, h=4.00045)
    fig.savefig(f'fig/{name}.pgf', bbox_inches='tight')


def runtime_plot():
    """
    Plot all runtimes
    """
    data = pd.read_csv(f'final/speedup.csv')
    data['time'] = data['time'] / 1000

    g = sns.catplot(
        data=data, kind='bar',
        x='Worker', y='time', hue='Methode',
        palette='deep', alpha=.9, height=9,
        legend=None
    )
    g.set_axis_labels('Worker', 'Zeit in s')
    plt.tight_layout()

    legend = plt.legend(loc='upper right')
    legend.set_title('Methode')

    fig = g.fig
    fig.set_size_inches(w=5.30045, h=3.30045)
    fig.savefig(f'fig/runtime.pgf', bbox_inches='tight')


# for latex
matplotlib.use('pgf')
matplotlib.rcParams.update({
    'pgf.texsystem': 'pdflatex',
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})

# Plots
comparison_plot('camera_result')
# comparison_plot('product_result')
# comparison_plot('existing_methods')
# comparison_plot('augmented_coef', x_ax='Koeffizient', hue='Datensatz')
# line_plot('soft_jaro_threshold')
# runtime_plot()
# speedup_plot()

# Non latex
# plt.show()