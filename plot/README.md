# Evaluation Plots

Final plots of the evaluation.

## Requirements

```bash
python -m venv .final-eval
source .final-eval/bin/activate  
pip install seaborn
```

## Usage
Evaluation results are already under `./final`. In `evaluation_plots.py` comment out needed plots:

```python
...

# Plots
# comparison_plot('camera_result')
# comparison_plot('product_result')
```
Plots will be generated in `./fig`
