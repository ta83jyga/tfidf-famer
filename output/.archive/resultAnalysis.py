import re
import matplotlib.pyplot as plt
import pandas as pd
from pandas import DataFrame

file = "tfidf-2020May-291539"
file1 = "tfidf-lt-2020May-312348.csv"
file2 = "newStfidf-07-2020Jun15-2154.csv"

show = ["threshold", "precision", "recall", "fMeasure", ]
# show = ["threshold", "fMeasure", ]


def plotData(data: DataFrame):
    data.plot(style=[':r', '--r', '-.r', ':b', '--b', '-.b', ])
    plt.axes().yaxis.grid(True)
    plt.show()


def compare(data1: DataFrame, data2: DataFrame):
    plotData(pd.concat([data1, data2], axis=1))


def getData(path: str):
    name = re.sub(r"2020.*.csv", "", path).replace("-", " ")
    return pd.read_csv(path)[show] \
        .set_index("threshold") \
        .rename(columns=lambda s: s + " " + name)


compare(getData(file1), getData(file2))
