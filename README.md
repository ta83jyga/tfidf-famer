# TF-IDF for Entity Resolution in huge Knowledge Graphs

Source code for my bachelor thesis **TF-IDF for Entity Resolution in huge Knowledge Graphs**. An Implementation and Evaluation of similarity measurement methods based on TF-IDF for Entity Resolution.
The module is implemented using the framework [FAMER](https://dbs.uni-leipzig.de/research/projects/object_matching/famer).

* Implementation of different TF-IDF normalisations.
* Implementation of Soft-TFIDF
* Effectivity and Speedup comparison against previous methods from FAMER.

## Structure
* `data` contains the benchmark datasets
* `src/main/java/org/gradoop/famer` implementation
    * `similarityMeasuring` the implemented similarity methods
    * `preprocessing/io/benchmarks` import for the camera benchmark datasets
    * `evaluation` import for the camera benchmark datasets
* `src/test` contains unit tests
* `output` contains the evaluation outputs
* `plot` contains python methods for plots used in the thesis

## Requirements

[Apache Flink](https://flink.apache.org/downloads.html) 1.7.2 or higher.

## Setup
Invoke the build at the root of the project. This will create an **uber** jar under `target` with all the dependencies.
```bash
./mvnw clean package
```

## Run Evaluation
For running the evaluation with a local Flink installation use:

```bash
/path/to/flink/bin/flink run ./target/TFIDF-FAMER-1.0.0-uber-jar [options]
```

| OPTIONS       | description                               | possible values / comments                                                                                  |
|:--------------|:------------------------------------------|:------------------------------------------------------------------------------------------------------------|
| **--dataset** | which dataset to evaluate                 | *ab*, *ag*, *pm*, *gs*, *pg*                                                                                |
| **--method**  | similarity measurement method             | *lt*, *at*, *nt*, *soft*, *ed*, *ej* , *jw*, default=*lt*                                                   |
| **--attr**    | attribute used for similarity measurement | *title* (for "ag","pm", "gs", "pg" ), *name* ("ab") , *description* ("ab", "ag"), *all*  ("pm", "gs", "pg") |
| **--coef**    | coefficient needed for some methods       | \[0-1\] used for methods soft, at, ej ,jw                                                                   |
| **--speedup** | measure speedup                           | boolean, default=false                                                                                      |
| **-p**        | flinks parallelism degree                 | \[0 - MAX_AVAILABLE_THREADS\]  used for speedup                                                             |
| **--data**    | path to benchmark datasets                | only if not executed from this directory                                                                    |

The method keys stand for:

| key  | name               |
|:-----|:-------------------|
| lt   | logarithmic TF-IDF |
| at   | augmented TF-IDF   |
| nt   | natural TF-IDF     |
| soft | Soft-TFIDF         |
| ej   | Extended-Jaccard   |
| jw   | Jaro-Winkler       |
| ed   | Edit-Distance      |

The datasets are

| key | dataset                                                                                                                |
|:----|:-----------------------------------------------------------------------------------------------------------------------|
| ab  | [Abt-Buy ](https://dbs.uni-leipzig.de/research/projects/object_matching/benchmark_datasets_for_entity_resolution)      |
| ag  | [Amazon-Google](https://dbs.uni-leipzig.de/research/projects/object_matching/benchmark_datasets_for_entity_resolution) |
| pm  | PriceMe                                                                                                                |
| gs  | GoSale                                                                                                                 |
| pg  | PriceMe and GoSale                                                                                                     |

*pm*, *gs* and *pg* are from the camera dataset of the [DI2KG](http://di2kg.inf.uniroma3.it/datasets.html) Challenge.